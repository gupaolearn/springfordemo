package com.wlhlearn.springdemo1.entity;

import lombok.Data;

/**
 * @Author: wlh
 * @Date: 2019/5/16 9:11
 * @Version 1.0
 * @despricate:learn
 */

@Data
public class Config {

    private  String className ;
    private  String beanName ;


}
