package com.wlhlearn.springdemo1.framework.beans;

/**
 * @Author: wlh
 * @Date: 2019/5/16 17:08
 * @Version 1.0
 * @despricate:learn
 */
public class WlhBeanPostProcessor {


    public Object postProcessBeforeInitialization(Object bean, String beanName) throws Exception {
        return bean;
    }


    public Object postProcessAfterInitialization(Object bean, String beanName) throws Exception {
        return bean;
    }
}
