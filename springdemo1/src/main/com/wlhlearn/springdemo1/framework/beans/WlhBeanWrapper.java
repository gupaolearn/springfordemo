package com.wlhlearn.springdemo1.framework.beans;

/**
 * @Author: wlh
 * @Date: 2019/5/16 11:39
 * @Version 1.0
 * @despricate:learn
 */
public class WlhBeanWrapper {

    private  Object  wrappedInstance  ;
    private Class<?> wrappedClass  ;

    public  WlhBeanWrapper(Object wrappedInstance){
        this.wrappedInstance=wrappedInstance ;
    }


    public  Object getWrappedInstance(){
        return  this.wrappedInstance ;
    }


    public  Class<?> getWrappedClass(){
         return  this.wrappedInstance.getClass() ;
    }
}
