package com.wlhlearn.springdemo1.framework.beans.config;

import lombok.Data;

/**
 * @Author: wlh
 * @Date: 2019/5/16 10:02
 * @Version 1.0
 * @despricate:learn
 */


@Data
public class WlhBeanDefinition {

      private  String  beanClassName ;
      private  Boolean lazyInit=false ;
      private  String  factoryBeanName ;

      private Boolean  isSingleton=true ;

}
