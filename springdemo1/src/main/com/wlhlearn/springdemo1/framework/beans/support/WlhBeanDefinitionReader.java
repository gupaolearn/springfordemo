package com.wlhlearn.springdemo1.framework.beans.support;

import com.wlhlearn.springdemo1.framework.beans.config.WlhBeanDefinition;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @Author: wlh
 * @Date: 2019/5/16 10:38
 * @Version 1.0
 * @despricate:learn
 */
public class WlhBeanDefinitionReader {

    private Properties config = new Properties();

    private final String scan_Package = "scanPackage";

    private List<String> registerBeanClasses = new ArrayList<>();

    public WlhBeanDefinitionReader(String... locations) {
        // 通过url 找到其对应的配置文件 转换为文件流
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(locations[0].replaceAll("classpath:", ""));
        try {
            config.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        doScanner(config.getProperty(scan_Package));
    }

    private void doScanner(String property) {

        URL url = this.getClass().getResource("/" + property.replaceAll("\\.", "/"));
        File classPath = new File(url.getFile());

        for (File file : classPath.listFiles()) {
            if (file.isDirectory()) {
                doScanner(property + "." + file.getName());
            } else {
                if (!file.getName().endsWith(".class")) {
                    continue;
                }
                String className = property + "." + file.getName().replace(".class", "");
                registerBeanClasses.add(className);
            }
        }

    }

    public Properties getConfig() {
        return this.config;
    }

    public List<WlhBeanDefinition> loadBeanDefinitions() {
        List<WlhBeanDefinition> result = new ArrayList<>();
        for (String className : registerBeanClasses) {
            WlhBeanDefinition wlhBeanDefinition = doCreateBeanDefinition(className);
            if (wlhBeanDefinition != null) {
                result.add(wlhBeanDefinition);
            }
        }
        return result;
    }

    private WlhBeanDefinition doCreateBeanDefinition(String className) {
        // classname  有可能对应的是一个接口  如果是接口的话 要实例化为其对应的实现类
        try {
            Class<?> beanClass = Class.forName(className);
            if (beanClass.isInterface()) {
                return null;
            }
            WlhBeanDefinition wlhBeanDefinition = new WlhBeanDefinition();
            wlhBeanDefinition.setBeanClassName(className);
            wlhBeanDefinition.setFactoryBeanName(beanClass.getSimpleName());
            return wlhBeanDefinition;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
