package com.wlhlearn.springdemo1.framework.beans.support;

import com.wlhlearn.springdemo1.framework.beans.config.WlhBeanDefinition;
import com.wlhlearn.springdemo1.framework.context.support.WlhAbstractApplicationContext;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author: wlh
 * @Date: 2019/5/16 9:56
 * @Version 1.0
 * @despricate:learn
 */
public class WlhDefaultListableBeanFactory extends WlhAbstractApplicationContext {


    /** Map of bean definition objects, keyed by bean name */
    //存储注册信息的BeanDefinition
    protected final Map<String, WlhBeanDefinition> beanDefinitionMap = new ConcurrentHashMap<>(256);

}
