package com.wlhlearn.springdemo1.framework.context;

import com.wlhlearn.springdemo1.framework.annotation.WlhAutowired;
import com.wlhlearn.springdemo1.framework.annotation.WlhController;
import com.wlhlearn.springdemo1.framework.annotation.WlhService;
import com.wlhlearn.springdemo1.framework.beans.WlhBeanPostProcessor;
import com.wlhlearn.springdemo1.framework.core.WlhBeanFactory;
import com.wlhlearn.springdemo1.framework.beans.WlhBeanWrapper;
import com.wlhlearn.springdemo1.framework.beans.config.WlhBeanDefinition;
import com.wlhlearn.springdemo1.framework.beans.support.WlhBeanDefinitionReader;
import com.wlhlearn.springdemo1.framework.beans.support.WlhDefaultListableBeanFactory;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author: wlh
 * @Date: 2019/5/16 9:45
 * @Version 1.0
 * @despricate: 顺序：IOC  DI   AoP  mvc
 */
public class WlhApplicationContext extends WlhDefaultListableBeanFactory implements WlhBeanFactory {

    private String[] configLocations;

    private WlhBeanDefinitionReader reader;

    // 单例的IOC 容器
    private Map<String, Object> singletonObjects = new ConcurrentHashMap<String, Object>();
    //  所有普通的IOC 容器
    private Map<String, WlhBeanWrapper> factoryBeanInstanceCache = new ConcurrentHashMap<String, WlhBeanWrapper>();

    public WlhApplicationContext(String... configs) {
        this.configLocations = configs;
        refresh();
    }

    @Override
    public void refresh() {
        //1  定位配置文件
        reader = new WlhBeanDefinitionReader(this.configLocations);

        //2  加载配置文件  扫描相关的类  把他们封装为BeanDinitaion
        List<WlhBeanDefinition> beanDefinitions = reader.loadBeanDefinitions();

        //3  注册  把配置信息放入容器里面（伪ioc容器）
        doRegisterBeanDefinitions(beanDefinitions);

        //4  把不是延时加载的类 要提前初始化
        doAutoried();
    }

    //  只处理非延时加载的情况
    private void doAutoried() {
        for (Map.Entry<String, WlhBeanDefinition> beanDefinitionEntry : super.beanDefinitionMap.entrySet()) {

            String beanName = beanDefinitionEntry.getKey();
            if (!beanDefinitionEntry.getValue().getLazyInit()) {
                try {
                    getBean(beanName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void doRegisterBeanDefinitions(List<WlhBeanDefinition> beanDefinitions) {

        for (WlhBeanDefinition beanDefinition : beanDefinitions) {
            super.beanDefinitionMap.put(beanDefinition.getFactoryBeanName(), beanDefinition);
        }

    }

    public Object getBean(Class<?> beanClass){
        try {
            return  getBean(beanClass.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null ;
    }

    public Object getBean(String beanName) throws Exception {

      WlhBeanPostProcessor beanPostProcessor=   new WlhBeanPostProcessor();
      beanPostProcessor.postProcessBeforeInitialization(null,null);
        //1  初始化
        WlhBeanWrapper beanWrapper = instantiateBean(beanName,this.beanDefinitionMap.get(beanName));
        if (this.factoryBeanInstanceCache.containsKey(beanName))
            throw new Exception("the " + beanName + "  is  exist");
        this.factoryBeanInstanceCache.put(beanName, beanWrapper);

        //2  注入
        populateBean(beanName, new WlhBeanDefinition(), beanWrapper);

        return this.factoryBeanInstanceCache.get(beanName).getWrappedInstance();
    }

    private WlhBeanWrapper instantiateBean(String beanName, WlhBeanDefinition wlhBeanDefinition) {
        //1 拿到要实例化的对象的类名
        String className = wlhBeanDefinition.getBeanClassName();
        //2 反射实例化  得到一个对象
        Object instance = null;
        try {
            if (this.singletonObjects.containsKey(className)) {
                instance = this.singletonObjects.get(className);
            } else {
                Class<?> clazz = Class.forName(className);
                instance = clazz.newInstance();
                this.singletonObjects.put(className, instance);
                this.singletonObjects.put(wlhBeanDefinition.getFactoryBeanName(), instance);
            }

        } catch (Exception e) {

        }
        //3 把对象封装到beanWrapper 中
        WlhBeanWrapper beanWrapper = new WlhBeanWrapper(instance);
        //4  把beanWrapper 存入ioc 容器中

        return beanWrapper;
    }

    private void populateBean(String beanName, WlhBeanDefinition wlhBeanDefinition, WlhBeanWrapper wlhBeanWrapper) {

        Object instance = wlhBeanWrapper.getWrappedInstance();

        Class<?> clazz = wlhBeanWrapper.getWrappedClass();
        if (!(clazz.isAnnotationPresent(WlhController.class) || clazz.isAnnotationPresent(WlhService.class))) {
            return;
        }

        // 获取所有的fields
        Field[] fields=clazz.getDeclaredFields();
        for (Field field:fields){
            if(!field.isAnnotationPresent(WlhAutowired.class)){
                continue;
            }
            WlhAutowired autowired=field.getAnnotation(WlhAutowired.class);

            String autowiredName=autowired.value().trim();
            if("".equals(autowiredName)){
                autowiredName=field.getType().getName() ;
            }
            field.setAccessible(true);
            try {
                field.set(instance,this.factoryBeanInstanceCache.get(beanName).getWrappedInstance());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }
    }


    public String[] getBeanDefinitions(){
        return  this.beanDefinitionMap.keySet().toArray(new String[this.beanDefinitionMap.size()]);
    }

    public int getBeanDefinitionCount(){
        return  this.beanDefinitionMap.size() ;
    }

    public Properties getConfig(){
        return  this.reader.getConfig();
    }
}
