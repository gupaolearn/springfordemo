package com.wlhlearn.springdemo1.framework.context;

/**
 * @Author: wlh
 * @Date: 2019/5/16 10:14
 * @Version 1.0
 * @despricate:一个通过解耦的方式获得IOC容器的顶层设计
 * 后面会通过一个监听器去扫描所有的类  只要实现了此接口 将自动调用setApplicationContext 方法
 * 从而将IOC容器注入目标类中
 */
public interface WlhApplicationContextAware {

    void setApplicationContext(WlhApplicationContext applicationContext);
}
