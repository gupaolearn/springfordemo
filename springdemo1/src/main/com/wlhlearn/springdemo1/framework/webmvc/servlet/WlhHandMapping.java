package wlhlearn.springdemo1.framework.webmvc.servlet;

import lombok.Data;

import java.lang.reflect.Method;
import java.util.regex.Pattern;

/**
 * @Author: wlh
 * @Date: 2019/5/17 9:59
 * @Version 1.0
 * @despricate:learn
 */
@Data
public class WlhHandMapping {

    private Object  controller ;

    private Method method ;

    private Pattern pattern ;

    public WlhHandMapping(Object controller, Method method, Pattern pattern) {
        this.controller = controller;
        this.method = method;
        this.pattern = pattern;
    }
}
