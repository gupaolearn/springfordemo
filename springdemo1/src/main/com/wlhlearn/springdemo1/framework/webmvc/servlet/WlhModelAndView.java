package wlhlearn.springdemo1.framework.webmvc.servlet;

import java.util.Map;

/**
 * @Author: wlh
 * @Date: 2019/5/17 11:20
 * @Version 1.0
 * @despricate:learn
 */
public class WlhModelAndView {

    private  String viewname ;

    private Map<String,?>  model ;

    public WlhModelAndView(String viewname) {
        this.viewname = viewname;
    }

    public WlhModelAndView(String viewname, Map<String, ?> model) {
        this.viewname = viewname;
        this.model = model;
    }

    public String getViewname() {
        return viewname;
    }

    public void setViewname(String viewname) {
        this.viewname = viewname;
    }

    public Map<String, ?> getModel() {
        return model;
    }

    public void setModel(Map<String, ?> model) {
        this.model = model;
    }
}
